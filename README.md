Simple FuelPHP CRUD example.

## Install
1. Clone the repo.
2. Add DB `products` on your local mysql.
3. Open console.
4. Run `php composer.phar self-update`. 
5. Run `php composer.phar update`.
6. Run `php oil refine migrate`.
