$(document).ready(function() {
    $('body').on('click', '.delete-product', function(e) {
        e.preventDefault();

        var r = confirm("Are you strong in your decision?");
        if (r === true) {
            var productId = $(this).data('id');

            $.ajax({
                url: '/delete',
                dataType: 'json',
                type: 'POST',
                data: { id: productId },
                success: function(response) {
                    console.log(response);
                    window.location.reload();
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log("textStatus: " + textStatus + " and errorThrown: " + errorThrown);
                }
            });
        }
    });

    $('#form_image').change(function() {
        $('#check-logo').removeClass('hidden');
    });
});