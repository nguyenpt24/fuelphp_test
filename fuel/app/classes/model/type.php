<?php

class Model_Type extends Orm\Model
{
    protected static $_properties = array('id', 'name');

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $_table_name = 'product_types';

    public static function getTypesList() {
        return self::find('all', array('order_by' => 'id'));
    }
}