<?php

class Model_Product extends Orm\Model
{
    protected static $_properties = array(
        'id',
        'product_type_id' => array(
            'data_type' => 'int',
            'label' => 'Product Type',
            'validation' => array('required'),
        ),
        'category_id' => array(
            'data_type' => 'int',
            'label' => 'Product Category',
            'validation' => array('required'),
        ),
        'name' => array(
            'data_type' => 'varchar',
            'label' => 'Product Name',
            'validation' => array('required', 'min_length' => array(3), 'max_length' => array(100)),
        ),
        'description' => array(
            'data_type' => 'varchar',
            'label' => 'Product Description',
            'validation' => array('required'),
        ),
        'image' => array(
            'data_type' => 'varchar',
            'label' => 'Product Logo',
            'validation' => array('required', 'max_length' => array(255)),
        ),
        'created_at',
        'updated_at'
    );

    protected static $_observers = array(
        'Orm\\Observer_Validation' => array(
            'events' => array('before_save')
        ),
        'Orm\\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => true,
        ),
        'Orm\\Observer_UpdatedAt' => array(
            'events' => array('before_save'),
            'mysql_timestamp' => true,
        ),
    );

    protected static $_table_name = 'products';

    protected static $_belongs_to = array(
        'category',
        'type' => array(
            'key_from' => 'product_type_id',
            'model_to' => 'Model_Type',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        )
    );

    public static function getProductById($id) {
        return self::find($id);
    }

    public static function getFullProductsInfo() {
        return self::find('all', array('related' => array('category', 'type'), 'order_by' => 'id'));
    }

    public static function getFullProductsInfoById($id) {
        return self::find($id, array('related' => array('category', 'type'), 'order_by' => 'id'));
    }

    public static function addProduct($product)
    {
        $config = array(
            'path' => DOCROOT.DS.'uploads',
            'max_size' => 1048576,
            'randomize' => true,
            'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
        );

        \Upload::process($config);

        if (\Upload::is_valid()) {
            \Upload::save();
            $value = \Upload::get_files();
            $product->image = '/uploads/'.$value[0]['saved_as'];
        }
        $product->save();

        return true;
    }

    public static function updateProduct($product)
    {
        $config = array(
            'path' => DOCROOT.DS.'uploads',
            'randomize' => true,
            'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
        );

        \Upload::process($config);

        if (\Upload::is_valid())
        {
            \Upload::save();
            $value = \Upload::get_files();
            $product->image = '/uploads/'.$value[0]['saved_as'];
        }

        $product->save();

        return true;
    }

    public static function getProductsByName($searchedText) {
        $results = array();
        $posts = self::find('all');

        $found_posts = \Search::find($searchedText)
            ->in($posts)
            ->by('name')
            ->relevance(80)
            ->execute();

        $posts = array();
        foreach($found_posts as $post) {
            array_push($posts, $post);
        }

        $results['message'] = count($posts).' results found';
        $results['searchedText'] = $searchedText;

        if (count($posts) > 0) {
            $results['results'] = $posts;
        }

        return $results;
    }
}