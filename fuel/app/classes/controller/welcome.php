<?php

class Controller_Welcome extends Controller_Template
{
    public $template = 'template';

    public function action_index()
    {
        $view = \View::forge('template');
        $view->content = \View::forge('welcome/index', array(
            'products' => \Model_Product::getFullProductsInfo()
        ));

        return $view;
    }
    public function action_test()
    {
        $this->template->content = View::forge('welcome/test');
    }

    public function action_404()
    {
        return \Response::forge(\Presenter::forge('welcome/404'), 404);
    }

    public function action_view($id)
    {
        $view = \View::forge('template');
        $view->content = \View::forge('welcome/view', array(
            'product' => \Model_Product::getFullProductsInfoById($id)
        ));

        return $view;
    }

    public function action_add()
    {
        $view = \View::forge('template');
        $view->content = \View::forge('welcome/add', array(
            'categories' => \Model_Category::getCategoryList(),
            'types' => \Model_Type::getTypesList()
        ));

        if (\Input::method() == 'POST') {
            $product = \Model_Product::forge(\Input::all());

            try {
                \Model_Product::addProduct($product);

                \Response::redirect('/');
            }
            catch(\Orm\ValidationFailed $e) {
                $view->content->set(array(
                    'fields' => $product,
                    'errors' => $e->getMessage()), null, false);
            }
        }

        return \Response::forge($view);
    }

    public function action_update($id)
    {
        $view = \View::forge('template');
        $view->content = \View::forge('welcome/update', array(
            'product' => \Model_Product::getProductById($id),
            'categories' => \Model_Category::getCategoryList(),
            'types' => \Model_Type::getTypesList()
        ));

        if (\Input::method() == 'POST' && \Input::post('id')) {
            $product = \Model_Product::find(\Input::post('id'))->set(\Input::all());

            try {
                \Model_Product::updateProduct($product);

                \Response::redirect('/');
            }
            catch(\Orm\ValidationFailed $e) {
                $view->content->set(array(
                    'product' => $product,
                    'errors' => $e->getMessage()), null, false);
            }
        }

        return \Response::forge($view);
    }

    public function action_delete()
    {
        if (\Input::method() == 'POST' && \Input::post('id')) {
            $product = \Model_Product::getProductById(\Input::post('id'));
            $product->delete();

            return true;
        }
    }

    public function action_search()
    {
        $data = array();
        $searchedText = \Input::post('searchedText');

        if (\Input::method() == 'POST' && $searchedText) {
            $data = \Model_Product::getProductsByName($searchedText);
        }

        $view = \View::forge('template');
        $view->content = \View::forge('welcome/search', $data);

        return $view;
    }
}
