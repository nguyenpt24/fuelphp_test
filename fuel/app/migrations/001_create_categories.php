<?php

namespace Fuel\Migrations;

class Create_categories
{
	public function up()
	{
		\DBUtil::create_table('categories', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'name' => array('constraint' => 100, 'type' => 'varchar'),
			'created_at' => array('type' => 'timestamp', 'default' => \DB::expr('CURRENT_TIMESTAMP')),
			'updated_at' => array('type' => 'timestamp', 'default' => \DB::expr('CURRENT_TIMESTAMP')),

		), array('id'), false, 'InnoDB', 'utf8_unicode_ci');

        \DB::insert('categories')->set(array('name' => 'fruits'))->execute();
        \DB::insert('categories')->set(array('name' => 'robots'))->execute();
        \DB::insert('categories')->set(array('name' => 'toys'))->execute();
        \DB::insert('categories')->set(array('name' => 'others'))->execute();
	}

	public function down()
	{
		\DBUtil::drop_table('categories');
	}
}