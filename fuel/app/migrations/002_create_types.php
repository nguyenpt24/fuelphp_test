<?php

namespace Fuel\Migrations;

class Create_types
{
	public function up()
	{
		\DBUtil::create_table('product_types', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'name' => array('constraint' => 100, 'type' => 'varchar'),
            'created_at' => array('type' => 'timestamp', 'default' => \DB::expr('CURRENT_TIMESTAMP')),
            'updated_at' => array('type' => 'timestamp', 'default' => \DB::expr('CURRENT_TIMESTAMP')),

		), array('id'), false, 'InnoDB', 'utf8_unicode_ci');

        \DB::insert('product_types')->set(array('name' => 'mobile'))->execute();
        \DB::insert('product_types')->set(array('name' => 'auto'))->execute();
        \DB::insert('product_types')->set(array('name' => 'other'))->execute();
	}

	public function down()
	{
		\DBUtil::drop_table('product_types');
	}
}