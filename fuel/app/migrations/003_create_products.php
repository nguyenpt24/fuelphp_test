<?php

namespace Fuel\Migrations;

class Create_products
{
    public function up()
    {
        \DBUtil::create_table('products', array(
            'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
            'product_type_id' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
            'category_id' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
            'name' => array('constraint' => 100, 'type' => 'varchar'),
            'description' => array('type' => 'text'),
            'image' => array('constraint' => 255, 'type' => 'varchar'),
            'created_at' => array('type' => 'timestamp', 'default' => \DB::expr('CURRENT_TIMESTAMP')),
            'updated_at' => array('type' => 'timestamp', 'default' => \DB::expr('CURRENT_TIMESTAMP')),

        ), array('id'), false, 'InnoDB', 'utf8_unicode_ci');

        \DBUtil::create_index('products', 'product_type_id', 'fk_products1');
        \DBUtil::create_index('products', 'category_id', 'fk_products2');

        \DBUtil::add_foreign_key('products', array(
            'constraint' => 'fk_products1',
            'key' => 'product_type_id',
            'reference' => array(
                'table' => 'product_types',
                'column' => 'id',
            ),
            'on_update' => 'CASCADE',
            'on_delete' => 'CASCADE'
        ));
        \DBUtil::add_foreign_key('products', array(
            'constraint' => 'fk_products2',
            'key' => 'category_id',
            'reference' => array(
                'table' => 'categories',
                'column' => 'id',
            ),
            'on_update' => 'CASCADE',
            'on_delete' => 'CASCADE'
        ));

        \DB::insert('products')->set(array(
            'product_type_id' => 1,
            'category_id' => 1,
            'name' => 'Apple',
            'description' => 'Inspired by Steve Jobs',
            'image' => '/uploads/apple.png'
        ))->execute();
        \DB::insert('products')->set(array(
            'product_type_id' => 1,
            'category_id' => 2,
            'name' => 'Android',
            'description' => 'It believes it can eat apple',
            'image' => '/uploads/android.png'
        ))->execute();
        \DB::insert('products')->set(array(
            'product_type_id' => 2,
            'category_id' => 3,
            'name' => 'Tesla',
            'description' => 'Elon Musk is so cutie!!',
            'image' => '/uploads/tesla.png'
        ))->execute();
        \DB::insert('products')->set(array(
            'product_type_id' => 3,
            'category_id' => 4,
            'name' => 'React',
            'description' => 'JS library',
            'image' => '/uploads/react.png'
        ))->execute();
    }

    public function down()
    {
        \DBUtil::drop_index('products', 'product_type_id');
        \DBUtil::drop_index('products', 'category_id');
        \DBUtil::drop_foreign_key('products', 'product_type_id');
        \DBUtil::drop_foreign_key('products', 'category_id');
        \DBUtil::drop_table('products');
    }
}