<?php
return array(
	'_root_'  => 'welcome/index',  // The default route
	'_404_'   => 'welcome/404',    // The main 404 route

	'add' => array('welcome/add', 'name' => 'add'),
	'delete' => array('welcome/delete', 'name' => 'delete'),
	'update/(:id)' => 'welcome/update/$1',
	'view/(:num)' => 'welcome/view/$1',
	'search' => array('welcome/search', 'name' => 'search'),
	'test' => array('welcome/test', 'name' => 'test'),
);
