<div class="container">
    <div class="update-form">
        <h3>Update product</h3>

        <?php echo Form::open(array('enctype' => 'multipart/form-data', 'action' => 'update/'.$product['id'], 'method' => 'post')); ?>
        <?php
            if(isset($errors)) {
                echo('<div class="error">'.$errors.'</div>');
            }
        ?>

        <div class="logo-preview">
            <img src="<?php echo($product->image); ?>"/>
        </div>

        <?php echo Form::hidden('id', $product['id']); ?>

        <div class="form-group file-item">
            <?php echo Form::label('New logo: 
                <br/>
                <span class="btn btn-warning">Choose a file...</span>
                <img src="/assets/img/check-icon.png" id="check-logo" class="'.(isset($image) ? "" : "hidden").'"/>',
                'image'); ?>
            <?php echo Form::file('image'); ?>
        </div>

        <div class="form-group">
            <?php echo Form::label('Name*:', 'name'); ?>
            <?php echo Form::input('name', $product['name'], array('class'=> 'form-control')); ?>
        </div>

        <div class="form-group">
            <?php
            $type = array(
                '' => 'Choose product type...'
            );

            if (isset($types)) {
                foreach($types as $key=>$value) {
                    $type[$value['id']] = $value['name'];
                }
            }
            ?>
            <?php echo Form::label('Type*:', 'product_type_id'); ?>
            <?php echo Form::select('product_type_id', $product['product_type_id'], $type, array('class'=> 'form-control')); ?>
        </div>

        <div class="form-group">
            <?php
            $category = array(
                '' => 'Choose category...'
            );

            if (isset($categories)) {
                foreach($categories as $key=>$value) {
                    $category[$value['id']] = $value['name'];
                }
            }
            ?>
            <?php echo Form::label('Category*:', 'category_id'); ?>
            <?php echo Form::select('category_id', $product['category_id'], $category, array('class'=> 'form-control')); ?>
        </div>

        <div class="form-group">
            <?php echo Form::label('Description*:', 'description'); ?>
            <?php echo Form::textarea('description', $product['description'], array('class'=> 'form-control')); ?>
        </div>

        <?php echo Form::submit('update_product', 'Update Product', array('class' => 'btn btn-primary')); ?>
        <a href="/" class="btn btn-primary">Cancel</a>

        <?php echo Form::close(); ?>
    </div>
</div>