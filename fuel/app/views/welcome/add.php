<div class="container">
    <div class="add-form">
        <h3>Add new product</h3>

        <?php echo Form::open(array('enctype' => 'multipart/form-data', 'action' => 'add', 'method' => 'post')); ?>
        <?php
            if(isset($errors)) {
                echo('<div class="error">'.$errors.'</div>');
            }
        ?>

        <div class="form-group file-item">
            <?php echo Form::label('Logo*: 
                <br/>
                <span class="btn btn-warning">Choose a file...</span>
                <img src="/assets/img/check-icon.png" id="check-logo" class="'.(isset($image) ? "" : "hidden").'"/>',
                'image'); ?>
            <?php echo Form::file('image'); ?>
        </div>

        <div class="form-group">
            <?php echo Form::label('Name*:', 'name'); ?>
            <?php echo Form::input('name', isset($fields['name']) ? $fields['name'] : '', array('class'=> 'form-control')); ?>
        </div>

        <div class="form-group">
            <?php
            $type = array(
                '' => 'Choose product type...'
            );

            if(isset($types)) {
                foreach($types as $key=>$value) {
                    $type[$value['id']] = $value['name'];
                }
            }
            ?>
            <?php echo Form::label('Type*:', 'product_type_id'); ?>
            <?php echo Form::select('product_type_id', isset($fields['product_type_id']) ? $fields['product_type_id'] : 'none', $type, array('class'=> 'form-control')); ?>
        </div>

        <div class="form-group">
            <?php
            $category = array(
                '' => 'Choose category...'
            );

            if(isset($categories)) {
                foreach($categories as $key=>$value) {
                    $category[$value['id']] = $value['name'];
                }
            }
            ?>
            <?php echo Form::label('Category*:', 'category_id'); ?>
            <?php echo Form::select('category_id', isset($fields['category_id']) ? $fields['category_id'] : 'none', $category, array('class'=> 'form-control')); ?>
        </div>

        <div class="form-group">
            <?php echo Form::label('Description*:', 'description'); ?>
            <?php echo Form::textarea('description', isset($fields['description']) ? $fields['description'] : '', array('class'=> 'form-control')); ?>
        </div>

        <?php echo Form::submit('add_product', 'Add Product', array('class' => 'btn btn-primary')); ?>

        <?php echo Form::close(); ?>
    </div>
</div>