<div class="container">
    <h3>All products</h3>

    <div class="table-wrapper">
        <div class="table">
            <div class="table-row">
                <div class="table-cell"><strong>Logo</strong></div>
                <div class="table-cell"><strong>Name</strong></div>
                <div class="table-cell"><strong>Category</strong></div>
                <div class="table-cell"><strong>Type</strong></div>
                <div class="table-cell"><strong>Description</strong></div>
                <div class="table-cell"><strong>#</strong></div>
            </div>
            <?php foreach ($products as $product) { ?>
                <div class="table-row">
                    <div class="table-cell">
                        <a href="/view/<?php echo($product->id); ?>"><img class="product-logo" src="<?php echo($product->image); ?>"/></a>
                    </div>
                    <div class="table-cell">
                        <?php echo($product->name); ?>
                    </div>
                    <div class="table-cell">
                        <?php echo($product->type->name); ?>
                    </div>
                    <div class="table-cell">
                        <?php echo($product->category->name); ?>
                    </div>
                    <div class="table-cell">
                        <?php echo($product->description); ?>
                    </div>
                    <div class="table-cell">
                        <a href="/update/<?php echo($product->id); ?>" class="btn btn-warning update-product">update</a>
                        <a href="#" class="btn btn-danger delete-product" data-id="<?php echo($product->id); ?>">delete</a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

    <a href="/add">+ Add new product</a>
</div>
