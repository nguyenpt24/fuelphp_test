<div class="container">
    <div class="search-form">
        <h3>Search product</h3>

        <?php echo Form::open(array('enctype' => 'multipart/form-data', 'action' => 'search', 'method' => 'post')); ?>

        <div class="form-group">
            <?php echo Form::input('searchedText', isset($searchedText) ? $searchedText : '', array('class'=> 'form-control search-field')); ?>
        </div>

        <?php echo Form::submit('search_product', 'Find it!', array('class' => 'btn btn-primary')); ?>

        <?php echo Form::close(); ?>
    </div>
    <div class="search-results">
        <?php
            if (isset($message)) {
                echo('<h3>'.$message.'</h3>');
            }

            if(isset($results)) {
                foreach($results as $res) {
                    echo('<h4><a href="/view/'.$res->id.'" target="_blank">'.$res->name.'</a></h4>');
                }
            }
        ?>
    </div>
</div>