<div class="container">
    <div class="view-form">
        <h3>View product</h3>

        <?php echo Form::open(array('enctype' => 'multipart/form-data', 'action' => 'update', 'method' => 'post')); ?>
        <?php
        isset($errors) && print_r($errors);
        ?>

        <div class="logo-preview">
            <img src="<?php echo($product->image); ?>"/>
        </div>

        <div class="form-group">
            <?php echo Form::label('Name:', 'name'); ?>
            <?php echo Form::input('name', $product->name, array('class'=> 'form-control', 'readonly' => true)); ?>
        </div>

        <div class="form-group">
            <?php echo Form::label('Type:', 'product_type_id'); ?>
            <?php echo Form::input('product_type_id', $product->type->name, array('class'=> 'form-control', 'readonly' => true)); ?>
        </div>

        <div class="form-group">
            <?php echo Form::label('Category:', 'category_id'); ?>
            <?php echo Form::input('category_id', $product->category->name, array('class'=> 'form-control', 'readonly' => true)); ?>
        </div>

        <div class="form-group">
            <?php echo Form::label('Description:', 'description'); ?>
            <?php echo Form::textarea('description', $product->description, array('class'=> 'form-control', 'readonly' => true)); ?>
        </div>

        <a href="/" class="btn btn-primary">To Home page</a>

        <?php echo Form::close(); ?>
    </div>
</div>