<html>
<head>
    <meta charset="utf-8">
    <title>FuelPHP CRUD</title>
    <?php echo Asset::css('bootstrap.css'); ?>
    <?php echo Asset::css('main.css'); ?>
</head>
<body>
<header>
    <div class="container">
        <a href="/"><div id="logo"></div></a>
        <a href="/search"><div id="search-link"></div></a>
    </div>
</header>

<?php echo $content; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<?php echo Asset::js('main.js'); ?>
</body>
</html>